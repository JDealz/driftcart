# DriftCart #

This repository contains the control system code for the DriftCart, one of Team Dadbot's entry into the Power Wheels Racing Series.

## Development Environment ##

* Visual Studio Code
* C/C++ plugin
* Arduino Plugin
* Test Suite works on Windows (MinGW) and MacOS(lldb)

## Hardware ##
* Arduino M0 Pro
* Hall-effect throttle input
* Hall-effect current sensor
* Shunt resistor-based current measurement
* Brushed Motor controller (PWM driven)

### Contribution guidelines ###

* Code should be unit tested before committing to master

### Who do I talk to? ###

* Kyle
* Joey
* Matt