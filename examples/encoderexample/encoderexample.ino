#include <Encoder.h>

// Red - 5V
// Black - GND
const int encoder_a = 2; // Green - pin 2 - Digital
const int encoder_b = 3; // White - pin 3 - Digital
volatile long encoder = 0;
long currentLoopTime = 0;
long oldLoopTime=0;
double gcDrivenWheelSpeedDivisions = 2450.0;
double drivenWheelspeed = 0;
long deltaT = 0;
double deltaTsec = 0;
double deltaTmin = 0;
long previous_encoder = 0;
long deltaEncoder = 0;

long newPosition = 0;
long oldPosition = 0;

Encoder myEnc(4, 5);

void setup() {
Serial.begin(9600);
}

void loop() {
//Serial.println(millis());
//Serial.print(',');
//Serial.print(deltaEncoder);
//Serial.print(',');
//Serial.println(deltaT);
Serial.println(drivenWheelspeed);
getDrivenWheelSpeed();
delay(20);
}


void getDrivenWheelSpeed()
{
 //wheel counts = pulses from interrupt
 //DRIVEN_WHEELDIVISIONS = # of magnets on wheel 

 newPosition = myEnc.read();

 long deltaEncoder = newPosition - oldPosition;
 
 double wheelrotations=(double)deltaEncoder/gcDrivenWheelSpeedDivisions;
 //#ifndef TEST_SUITE
 currentLoopTime = millis();
 //#else
 //int currentLoopTime = 0;
 //#endif
 deltaT= currentLoopTime - oldLoopTime;
 deltaTsec = (double)deltaT/1000.0;
 deltaTmin = (double)deltaTsec/60.0;
 drivenWheelspeed = wheelrotations/deltaTmin;
 
 oldLoopTime=currentLoopTime;
 //currentLoopTime = 0;
 //do we need to have concern about hitting interrupts while this code is calculating and missing pulses??

 oldPosition = newPosition;

//deltaT = deltaTsec= deltaTmin = 0;

// return drivenWheelspeed;
}
