/** @file fusemonitor.cpp
 *  @brief Definition of Fuse Monitor functions
 * 
 * This file contains the declaration of functions used by the fuse monitoring algorithms.
 * 
 * This module is used to collect and analyze data about the state of the fuse. 
 */

#include "fusemonitor.h"
#include "scaling.h"


T_VOLT gvFuseVoltageDrop = 0.0;

void InitializeFuseMonitor(void)
{

}


void MonitorFuse(void)
{
    gvFuseVoltageDrop = getFuseVoltageDrop();
}

