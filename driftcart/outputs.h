#ifndef OUTPUTS_H
#define OUTPUTS_H

/** @file outputs.h
 *  @brief Function prototypes for the controller outputs.
 *  
 * @note 
 * All write functions accept "safe" data. 
 * This means that data passed to these functions is ready
 * to pass to the hardware
 *
 */



#include "types.h"

/**********************/
/**** Preprocessor ****/
/**********************/


/**********************/
/** Global Variables **/
/**********************/

/** \brief The lower limit of what can be commanded for a PWM throttle output
 */
extern T_PWM_DUTY_CYCLE gcPWMLowerBound;

/** \brief The upper limit of what can be commanded for a PWM throttle output
 */
extern T_PWM_DUTY_CYCLE gcPWMUpperBound;

/**
 * \brief The commanded throttle output PWM (0-255) counts
 */
extern T_ANALOG_RAWOUT gvCmdThrottlePWM;

/**********************/
/*** Init Functions ***/
/**********************/

/**
 * \brief Initialize the outputs module. Called from Setup.
 */
extern void InitializeOutputs(void);

/**********************/
/*** Write Functions **/
/**********************/



/** \brief Writes the safe, scaled motor speed to the hardware.
 */
extern void writeMotorThrottleDAC(T_ANALOG_RAWOUT);

#endif /* OUTPUTS_H */