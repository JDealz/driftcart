/** @file brakingsystem.cpp
 *  @brief Definition of Braking System
 * 
 * This module is used to determing braking status.
 */

#include "brakingsystem.h"
#include "inputs.h"

bool gvBrakeApplied = true;


void InitializeBrakingSystem(void)
{
    //attachInterrupt(WHEELPULSE_PIN,countwheelpulse,RISING);
    //
}

void MonitorBrakingStatus(void)
{
    gvBrakeApplied = readBrakeSwitch();
}

bool getBrakeApplied(void)
{
    return gvBrakeApplied;
}