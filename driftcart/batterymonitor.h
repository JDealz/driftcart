#ifndef BATTERYMONITOR_H
#define BATTERYMONITOR_H

/** @file batterymonitor.h
 *  @brief Declaration of Battery Monitor functions.
 * 
 * This file contains the declaration of functions and variables used by the battery monitoring system. 
 * This module is used primarily to enable remedial actions (such as propulsion disable) to protect the health of batteries.
 */

#include "types.h"

/**********************/
/**** Preprocessor ****/
/**********************/

/**********************/
/** Global Variables **/
/**********************/

/** \brief Indicates true if the battery is in a low state of charge
 */
extern bool gvLowBatteryAlarmActive;

/** \brief Indicates true if the battery is in a critically low state of charge
 */
extern bool gvCriticalBatteryAlarmActive;

/** \brief Indicates the current battery level in Volts
 */
extern T_VOLT gvBatteryVoltage;

/**********************/
/**** Calibrations ****/
/**********************/

/** \brief True if the batteries in use are Lithium Polymer. Used in battery monitoring and health preservation
 */
extern bool gcBatteryChemistryLipo;

/** \brief The voltage below which a single cell of a battery is considered to be in a low state of charge
 */
extern T_VOLT gcLowCellLimit;

/** \brief The voltage below which a single cell of a battery is considered to be in a critically low state of charge
 */
extern T_VOLT gcCritcalCellLimit;

/** \brief The number of cells (in series) in the battery.
 */
extern int gcNumberOfCellsInBattery;

/**********************/
/*** Init Functions ***/
/**********************/

/** \brief Initialize the battery monitoring system. Called from Setup function.
 */
extern void InitializeBatteryMonitor(void);

/**********************/
/** Global Functions **/
/**********************/

/** \brief Monitor the battery voltage. This is called by the scheduler.
 */
extern void MonitorBatteryVoltage(void);

/**********************/
/**** Get Functions ***/
/**********************/

/** \brief Determines the state of the Critical battery alarm
 *  \return True if the Critical battery alarm is active
 */
extern bool getCriticalBatteryAlarmActive(void);

/** \brief Determines the state of the Low battery alarm
 *  \return True if the Low battery alarm is active
 */
extern bool getLowBatteryAlarmActive(void);

#endif /* BATTERYMONITOR_H */