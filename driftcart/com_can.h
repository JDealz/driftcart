#ifndef COM_CAN_H
#define COM_CAN_H

/** @file com_can.h
 *  @brief Function prototypes for the CAN communications subsystem.
 */

#include "types.h"

/**********************/
/**** Preprocessor ****/
/**********************/

/**********************/
/** Global Variables **/
/**********************/

/** \brief CAN Signal received
 * Type: bool
 * Name: Signal1
 */
extern bool gvCANRX_Signal1;

/** \brief CAN Signal received
 * Type: bool
 * Name: Signal2
 */
extern bool gvCANRX_Signal2;

/** \brief CAN Signal received
 * Type: int
 * Name: Signal3
 */
extern int gvCANRX_Signal3;

/** \brief CAN Signal transmitted
 * Type: bool
 * Name: Signal1
 */
extern bool gvCANTX_Signal1;

/** \brief CAN Signal transmitted
 * Type: bool
 * Name: Signal2
 */
extern bool gvCANTX_Signal2;

/** \brief CAN Signal transmitted
 * Type: int
 * Name: Signal3
 */
extern int gvCANTX_Signal3;

/**********************/
/*** Init Functions ***/
/**********************/

/** \brief Initializes the CAN subsystem. Called from Setup function.
 */
extern void InitializeCAN(void);

/**********************/
/**** CAN Functions ***/
/**********************/

extern void BuildAndSendCANFrames(void);

extern void ReceiveAndParseCANFrames(void);


#endif /* COM_CAN_H */