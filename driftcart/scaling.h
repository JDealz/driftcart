#ifndef SCALING_H
#define SCALING_H

/** @file scaling.h
 *  @brief Function prototypes for the initial scaling.
 */

#include "types.h"


/**********************/
/**** Preprocessor ****/
/**********************/


/**********************/
/** Global Variables **/
/**********************/

extern T_PCT gvThrottleOutputCommanded;

extern T_AMP gvCurrentHall;

/**********************/
/**** Calibrations ****/
/**********************/

extern T_RATIO gcThrottleOutputGain;

/**
 * \brief Arduino Voltage Reference
 */
extern T_VOLT gcVoltRef;

/**
 * \brief Gain of the voltage divider on the main battery voltage monitor
 */
extern T_GAIN gcMainBatteryVoltageGain;

/**
 * \brief Minimum counts
 */
extern T_ADC_COUNTS gcMinADCValue;

/**
 * \brief Counts for 12 Bit A/D channel
 */
extern T_ADC_COUNTS gcMaxADCValue12Bit;

/**
 * \brief Counts for 10 Bit A/D channel
 */
extern T_ADC_COUNTS gcMaxADCValue10Bit;

/**
 * \brief Counts for 8 Bit A/D channel
 */
extern T_ADC_COUNTS gcMaxADCValue8Bit;

/**
 * \brief The Gain set by Rgain of the current shunt instrumentation amp
 */
extern T_GAIN gcShuntCurrentAmpGain;

/**
 * \brief The Current Shunt volt output for 1 amp (sensor is 0.00075 ohms)
 */
extern T_OHMS gcShuntCurrentResistance;

/**
 * \brief The ADC Value (counts) from the current shunt sensor @ 0 amps (~1.5 volts or 500)
 */
extern T_ADC_COUNTS gcShuntCurrentCountsAtZeroCurrent;

/**
 * \brief The Hall effect current sensor "gain" Amps per volt (~10 amps per volt)
 */
extern T_RATIO gcHallCurrentAmpsPerVolt;

/**
 * \brief The ADC Value (counts) from the hall effect current sensor @ 0 amps (~1.5 volts or 500)
 */
extern T_ADC_COUNTS gcHallCurrentCountsAtZeroCurrent;

/**
 * \brief The Lower Bound of the Motor Speed.
 */
extern T_RPM gcMotorSpeedLimit_Min;

/**
 * \brief The Upper Bound of the Motor Speed - specific to Brushed MY1020.
 */
extern T_RPM gcMotorSpeedLimit_Max;

/**
 * \brief The Lower Bound of the Throttle value expected from somewhere else. 
 */
extern T_PCT gcThrottleInputLimit_Min;

/**
 * \brief The Upper Bound of the Throttle value expected from somewhere else.
 */
extern T_PCT gcThrottleInputLimit_Max;

/**
 * \brief The Lower Bound of the Throttle output percent value
 */
extern T_PCT gcThrottleOutputLimit_Min;

/**
 * \brief The Upper Bound of the Throttle output percent value
 */
extern T_PCT gcThrottleOutputLimit_Max;

/**
 * \brief The Lower Bound of the Throttle value expected from the ADC. 
 */
extern T_ADC_COUNTS gcRawThrottleInputLimit_Min;

/**
 * \brief The Upper Bound of the Throttle value expected from the ADC.
 */
extern T_ADC_COUNTS gcRawThrottleInputLimit_Max;

/**
 * \brief 
 */
extern T_DAC_COUNTS gcDACValueMin;

/**
 * \brief 
 */
extern T_DAC_COUNTS gcDACValueMax;

/**
 * \brief The Lower Bound of the Throttle output PWM value
 */
extern T_PWM_DUTY_CYCLE gcPWMThrottleOutputLimit_Min;

/**
 * \brief The Upper Bound of the Throttle output PWM value
 */
extern T_PWM_DUTY_CYCLE gcPWMThrottleOutputLimit_Max;

/**
 * \brief The Lower Bound of the hall effect current value expected from the ADC
 */
extern T_ADC_COUNTS gcHallCurrentInputLimit_Min;

/**
 * \brief The Upper Bound of the hall effect current value expected from the ADC
 */
extern T_ADC_COUNTS gcHallCurrentInputLimit_Max;

/**
 * \brief The number of magnets/ divisions on a wheel for wheelspeed calc
 */
extern int gcDrivenWheelSpeedDivisions;

/**
 * \brief 
 */
extern unsigned int gcPotentiometerMinValueRaw;

/**
 * \brief 
 */
extern unsigned int gcPotentiometerMaxValueRaw;

/** The number of magnets/ divisions on a wheel for wheelspeed calc */
#define DRIVEN_WHEELDIVISIONS 3

/**********************/
/*** Init Functions ***/
/**********************/

extern void InitializeScaling(void);


/**********************/
/**** Get Functions ***/
/**********************/

/*
 * Note: All get functions shall return "safe" data. 
 * This means that data returned from these functions should be able to be used
 * in algorithms without fear of the data causing unwanted side effects.
 */


/** \brief Gets the clipped and scaled throttle value.
 *  \return PCT.
 */
extern T_PCT getThrottle(void);

/** \brief Returns the driven wheelspeed calculated based on interrupt counts and loop time
 *  \return RPM
 */
extern T_RPM getDrivenWheelSpeed(void);

/** \brief Returns the curent current based on reading from Hall effect sensor
 *  \return RPM.
 */
extern T_AMP getCurrentHall(void);

/** \brief Returns the curent current based on reading from shunt sensor
 *  \return Amps.
 */
extern T_AMP getCurrentShunt(void);

/** \brief Returns the battery voltage
 *  \return Volts.
 */
extern T_VOLT getBatteryVoltage(void);

/** \brief Returns the voltage drop across the fuse.
 *  \return Volts.
 */
extern T_VOLT getFuseVoltageDrop(void);

/** \brief Returns the voltage on the analogread pin, can account for sensor gain
 */
extern float counts2Volts(int counts, int channelresolution, float gain);

/**********************/
/**** Set Functions ***/
/**********************/

/*
 * Note: All set functions shall pass "safe" values to the outputs layer and 
 * on to the hardware, even if the input is "unsafe".
 */

/** \brief Sets the motor speed (in RPMs) by scaling and clipping it to a safe value before writing to the hardware. 
 */
extern void setMotorSpeed(T_RPM, T_MotorType, T_MotorControlType);

 /** \brief Sets the motor speed 
  */
extern void setMotorThrottle(T_PCT, T_MotorType, T_MotorControlType);

/**********************/
/** Utility Functions */
/**********************/

/** \brief Scales or "maps" a signal from one range to a new range. 
 */
extern int scaleToRange(int, int, int, int, int);


/** \brief Scales or "maps" a signal from one range to a new range. 
 */
extern double scaleToRange(double, double, double, double, double);

/** \brief Clips a signal to an upper and lower bound. 
 */
extern int clip(int, int, int);

/** \brief Clips a signal to an upper and lower bound. 
 */
extern double clip(double, double, double);

#endif /* SCALING_H */