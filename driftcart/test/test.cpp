// This is only a test program. This is not run on the Arduino. --KAP

#include <iostream>
#include "../scaling.h"
#include "../brushedmotorcontrol.h"
#include <fstream>
#include "arduinosimulator.h"

using namespace std;

int main(int argv, char** argc)
{

    int result = scaleToRange(100, 0, 1024, 0, 100);

    cout << "Scale to range (100, 0, 1024, 0, 100) = " << result << endl;

    simulator_init_logging("throttleResponse.csv");

    int analogpins=3;
    for(double i = 0; i < 21; i++)
    {
        //secondary loop to hit all analog pins with the read sweep
        for(int n=0; n<analogpins;n++)
        {
            simulator_set_analog_value(n, i*50);
        }
 //       double temp=(i*50);
  //      point(x, ymotorPWM, i, temp);
        
        ManageMotorSpeed();
    }

    simulator_close_logging();

    return 0;
}
