/** @file arduinosimulator.cpp
 *  @brief Function stubs for arduino IO used in testing
 *
 *
 *  @author Kyle Preiksa
 *  @bug Literally nothing works.
 */

#include "arduinosimulator.h"
#include <fstream>

std::ofstream currentCSV;

void simulator_init_logging(std::string fileName)
{
    if (fileName != "")
    {
        currentCSV.open(fileName.c_str());
    }
    else
    {
        currentCSV.open("Default.csv");
    }
    
}

void simulator_close_logging()
{
    currentCSV.close();
}

int analogValue[16] = {0};

void simulator_set_analog_value(int pin, int value)
{
    analogValue[pin] = value;
}

int analogRead(int pinNumber)
{
    // Actually do something in here to return a random/sampled value.
    int value = analogValue[pinNumber];

    std::cout << "SIMULATOR - ANALOG_READ: Getting PIN #" << pinNumber << ". Current Value=" << value << std::endl;

    currentCSV << value << ",";

    return value;
}

void analogWrite(int pinNumber, int value)
{
    std::cout << "SIMULATOR - ANALOG_WRITE: Setting PIN #" << pinNumber << " to VALUE=" << value << std::endl;
    currentCSV << value << std::endl;
}


void digitalWrite(int pinNumber, int value)
{
    std::cout << "SIMULATOR - DIGITAL_WRITE: Setting PIN #" << pinNumber << " to VALUE=" << value << std::endl;
}

int digitalRead(int pinNumber)
{
    int value = 13;
    std::cout << "SIMULATOR - DIGITAL_READ: Getting PIN #" << pinNumber << ". Current Value=" << value << std::endl;
    return value;
}