/** @file arduinosimulator.h
 *  @brief Function prototypes for the IO simulator
 *
 *
 *  @author Kyle Preiksa
 *  @bug No known bugs.
 */

#include <iostream>
#include <string>

#ifndef ARDUINOSIMULATOR_H
#define ARDUINOSIMULATOR_H

#define ADC_BITS 12
#define DAC_BITS 10
#define PWM_BITS 10

void simulator_init_logging(std::string);

void simulator_close_logging();

void simulator_set_analog_value(int, int);

/** \brief Gets a simulated ADC value.
 *
 * \return The ADC Value
 */
int analogRead(int);

/** \brief Sets a simulated PWM value.
 *
 */
void analogWrite(int, int);

/** \brief Sets a simulated digital value.
 *
 */
void digitalWrite(int, int);

/** \brief Gets a simulated digital value.
 *
 * \retun The digital value
 */
int digitalRead(int);

#endif