#ifndef COM_BITBANG_SPI_H
#define COM_BITBANG_SPI_H

/** @file com_bitbang_spi.h
 *  @brief Function prototypes for the bit banged SPI communications subsystem.
 * 
 * This module enables SPI communications to occur without using the built-in SPI peripheral. 
 * This has the advantage of configurability and the ability to use any pin, at the expense of speed.
 */

#include "types.h"
#include "Arduino.h"

/**********************/
/**** Preprocessor ****/
/**********************/

/**********************/
/** Global Variables **/
/**********************/

/**********************/
/*** Init Functions ***/
/**********************/

/**********************/
/** Global Functions */
/**********************/

/** \brief Initializes the secondary SPI subsystem (bitbanging rather than using the SPI peripheral). Called from Setup function.
 */
extern void InitializeBitBangSPI(void);

/** \brief Sets Chip Select to the active state for the Digital Potentiometer
 */
extern void SetChipSelectActive(void);

/** \brief Sets Chip Select to the inactive state for the Digital Potentiometer
 */
extern void SetChipSelectInactive(void);

/** \brief Sends a single byte by shifting the bits out MSB first.
 *  \param The Byte to send
 */
extern void bitbangSPIByte(byte b);


#endif /* COM_BITBANG_SPI_H */