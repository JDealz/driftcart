/** @file batterymonitor.cpp
 *  @brief Definition of Battery Monitor functions
 * 
 * This file contains the definitions of the battery monitoriing system.
 * This module is used primarily to enable remedial actions (such as propulsion disable) to protect the health of batteries.
 */

#include "batterymonitor.h"
#include "scaling.h"


bool gvLowBatteryAlarmActive = false;

bool gvCriticalBatteryAlarmActive = false;

bool gcBatteryChemistryLipo = true;

T_VOLT gcLowCellLimit = 3.9; // this likely needs tweaked to the correct value.

T_VOLT gcCritcalCellLimit = 3.7; // this likely needs tweaked to the correct value.

int gcNumberOfCellsInBattery = 5; // this needs set to 5 for our small batteries and 6 for the big batteries

T_VOLT gvBatteryVoltage = 0.0;


void InitializeBatteryMonitor(void)
{
    //attachInterrupt(WHEELPULSE_PIN,countwheelpulse,RISING);
    //
}

void MonitorBatteryVoltage(void)
{
    // Get the voltage
    gvBatteryVoltage = getBatteryVoltage();
    
    // using the calibrations for cell voltage and # of cells, determine the state of any alarms
    if (gcBatteryChemistryLipo == true)
    {
        if((gvBatteryVoltage < gcLowCellLimit * gcNumberOfCellsInBattery) && (gvBatteryVoltage > gcCritcalCellLimit * gcNumberOfCellsInBattery)){
            gvLowBatteryAlarmActive = true;
            gvCriticalBatteryAlarmActive = false;
        }
        if(gvBatteryVoltage < gcCritcalCellLimit * gcNumberOfCellsInBattery){
            gvCriticalBatteryAlarmActive = true;
            gvLowBatteryAlarmActive = true;
        }

    }

}


bool getCriticalBatteryAlarmActive(void){
    return gvCriticalBatteryAlarmActive;
}

bool getLowBatteryAlarmActive(void){
    return gvLowBatteryAlarmActive;
}