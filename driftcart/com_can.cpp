/** @file com_can.cpp
 *  @brief Function definitons for the CAN communications subsystem.
 */
#include "com_can.h"
#include "types.h"
#include "Arduino.h"


bool gvCANRX_Signal1;
bool gvCANRX_Signal2;
int gvCANRX_Signal3;

bool gvCANTX_Signal1;
bool gvCANTX_Signal2;
int gvCANTX_Signal3;

void InitializeCAN(void)
{

}

void BuildAndSendCANFrames(void)
{   
    //BuildMessage1FE();
    //SendMessage 

}

void ReceiveAndParseCANFrames(void)
{
    //ReceiveMessage
    byte bytebuf[8] = {};
    int rxlen = 8;
    //ParseMessage1FF(bytebuf, rxlen);
    //ParseMessage gvCANRX_SignalName

}

void BuildMessage1FE(void)
{
    byte intVal1 = 0;
    if(gvCANTX_Signal1 == true)
    {
        intVal1 = 1;
    }

    byte intVal2 = 0;
    if(gvCANTX_Signal2 == true)
    {
        intVal2 = 1;
    }

    byte byte0 = 0b000000| ((intVal2 << 1) & 0b00000010 ) | (intVal1 & 0b00000001);

    byte byte1 = 0;
    byte byte2 = 0;
    byte byte3 = 0;
    byte byte4 = 0;

    byte1 = ((gvCANTX_Signal3 & 0xFF000000) >> 24);
    byte2 = ((gvCANTX_Signal3 & 0x00FF0000) >> 16);
    byte3 = ((gvCANTX_Signal3 & 0x0000FF00) >> 8);
    byte4 = ((gvCANTX_Signal3 & 0x000000FF));
}

void ParseMessage1FF(byte* buf, int buflen)
{
    /* Byte 0: Signal1 */
    byte byte0 = *(buf + 0);

    byte byte1 = *(buf + 1);
    byte byte2 = *(buf + 2);
    byte byte3 = *(buf + 3);
    byte byte4 = *(buf + 4);

    gvCANRX_Signal1 = (1== (byte0 & 0b00000001));
    gvCANRX_Signal2 = (1 == (byte0 & 0b00000010) >> 1);


    gvCANRX_Signal3 = ((byte1 << 24) & 0xFF000000) | ((byte2 << 16) & 0x00FF0000)  | ((byte3 << 8) & 0x0000FF00) | (byte4 & 0x000000FF); //reconstruct int from 4 bytes


}