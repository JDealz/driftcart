/** @file currentmonitor.cpp
 *  @brief Definition of Current Monitor functions
 * 
 * This file contains the definitions of the current monitoriing algorightms.
 * 
 * This module is used to collect data about the amount of current being drawn from the batteries,
 * which will directly affect fuse breakdown.
 */

#include "currentmonitor.h"
#include "scaling.h"

T_AMP gvHallEffectCurrentReading = 0.0;

T_AMP gvShuntCurrentReading = 0.0;

T_AMP gvArbitratedCurrentReading = 0.0;


void InitializeCurrentMonitorSystem(void)
{
    //attachInterrupt(WHEELPULSE_PIN,countwheelpulse,RISING);
    //
}

void MonitorBatteryCurrent(void)
{
    MonitorHallEffectCurrent();
    MonitorShuntCurrent();
    MonitorTotalCurrent();
}

void MonitorHallEffectCurrent(void)
{
    gvHallEffectCurrentReading = getCurrentHall();

}

void MonitorShuntCurrent(void)
{
    gvShuntCurrentReading = getCurrentShunt();

}

void MonitorTotalCurrent(void)
{
    if(gvHallEffectCurrentReading > gvShuntCurrentReading){
        gvArbitratedCurrentReading = gvHallEffectCurrentReading;
    }
    else
    {
        gvArbitratedCurrentReading = gvShuntCurrentReading;
    }

}


T_AMP getHallEffectCurrentReading(void)
{
    return gvHallEffectCurrentReading;
}

T_AMP getShuntCurrentReading(void)
{
    return gvShuntCurrentReading;
}

T_AMP getArbitratedCurrentReading(void)
{
    return gvArbitratedCurrentReading;
}