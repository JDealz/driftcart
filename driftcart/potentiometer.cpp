/** @file potentiometer.cpp
 *  @brief Function definitons for the digital potentiometer
 */
#include "potentiometer.h"
#include "types.h"
#include "Arduino.h"
#include "com_bitbang_spi.h"
#include "scaling.h"

int gvPotentiometerValue = 0;

void InitializePotentiometer(void)
{
    SetChipSelectInactive();
}

void setPotentiometerValue(unsigned int value)
{
    SetChipSelectActive();

    byte trimmedValue = 0;
    if(value < gcPotentiometerMinValueRaw){
        trimmedValue = gcPotentiometerMinValueRaw;
    }
    else if (value > gcPotentiometerMaxValueRaw)
    {
        trimmedValue = gcPotentiometerMaxValueRaw; //256 is the max value accepted by the potentiometer (9 bits)
    }
    else{
        trimmedValue = value;
    }

    gvPotentiometerValue = trimmedValue;

    /* 
     * The structure of the data being sent is as follows... 
     * A3 : A0 is the address of the register being written to (see table 7-2 in the datasheet)
     * C1 : C0 is the command (see table 7-1 in the datasheet)
     * Where D9-D0 is binary representation of 0-256
     * This # represents (Rpot/256)*Data ohms measured across the wiper
     * |------Command Byte-------| |--------Data Byte--------|
     * | A3 A2 A1 A0 C1 C0 D9 D8 | | D7 D6 D5 D4 D3 D2 D1 D0 | 
     * 
     * These are sent MSB first, command byte first.
    */

    byte commandByte = 0b00000000 & ((trimmedValue & 0b100000000) >> 8);
    byte dataByte = trimmedValue & 0b11111111;

    // write to address 0x00 (the first potentiometer)
    bitbangSPIByte(commandByte); // combine the command byte with the MSB of the data.

    bitbangSPIByte(dataByte); // the bottom 8 bytes

    SetChipSelectInactive();

}