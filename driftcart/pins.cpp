/** @file pins.cpp
 *  @brief Function definitons for the controller pins.
 */

#include "pins.h"
#ifndef TEST_SUITE
#include <Arduino.h>
#else
#include "test/arduinosimulator.h"
#endif

/**********************/
/** Global Variables **/
/**********************/

/*
 * Analog Output (one available on Arduino M0 Pro)
 */
T_PIN gcPin_DACThrottleOut = A0;

/*
 * Analog Inputs
 */
T_PIN gcPin_HallEffectThrottleIn = A1;
T_PIN gcPin_CurrentSensorHallEffectIn = A2;
T_PIN gcPin_CurrentShuntAmpedIn = A3;
T_PIN gcPin_FuseVDropAmpedIn = A4;
T_PIN gcPin_TractiveBatteryVoltageSensorIn = A5;

/*
 * Digital Inputs
 */
T_PIN gcPin_ReverseSwitchIn = 13;
T_PIN gcPin_BrakeSwitchIn = 12;
T_PIN gcPin_WheelSpeedEncoderAIn = 11;
T_PIN gcPin_WheelSpeedEncoderBIn = 10;

/*
 * Fake SPI for Digipot
 */ 
T_PIN gcPin_DigiPotSerialData = 9;
T_PIN gcPin_DigiPotClock = 8;

/*
 * Digital Output
 */
T_PIN gcPin_PWMMotorControl5VBoostedOut = 7;
T_PIN gcPin_PWMMotorControlArbOut = 6;
T_PIN gcPin_SDCardChipSelectOut = 5;
T_PIN gcPin_CANChipSelectOut = 4;

/*
 * Digipot Chip Select
 */
T_PIN gcPin_DigiPotCS = 3;

/*
 * Digital Input for CAN Interrupt
 */ 
T_PIN gcPin_CANInterruptIn = 2;

/*
 * UART
 */
T_PIN gcPin_RxD = 1;
T_PIN gcPin_TxD = 0;


/**********************/
/* Function Definitons */
/**********************/

void InitializePins(void)
{
    InitalizeInputPins();
    InitalizeOutputPins();
}


void InitalizeInputPins(void)
{
    //TODO: Need to determine if these should be INPUT_PULLUP, INPUT_PULLDOWN, or just INPUT

    // Analog
    pinMode(gcPin_HallEffectThrottleIn, INPUT);
    pinMode(gcPin_CurrentSensorHallEffectIn, INPUT);
    pinMode(gcPin_CurrentShuntAmpedIn, INPUT);
    pinMode(gcPin_FuseVDropAmpedIn, INPUT);
    pinMode(gcPin_TractiveBatteryVoltageSensorIn, INPUT);

    // Digital
    pinMode(gcPin_ReverseSwitchIn, INPUT);
    pinMode(gcPin_BrakeSwitchIn, INPUT);
    pinMode(gcPin_WheelSpeedEncoderAIn, INPUT);
    pinMode(gcPin_WheelSpeedEncoderBIn, INPUT);

}

void InitalizeOutputPins(void)
{

    // Analog
    pinMode(gcPin_DACThrottleOut, OUTPUT);



    // Digital

    pinMode(gcPin_PWMMotorControl5VBoostedOut, OUTPUT);
    pinMode(gcPin_PWMMotorControlArbOut, OUTPUT);
    pinMode(gcPin_SDCardChipSelectOut, OUTPUT);
    pinMode(gcPin_CANChipSelectOut, OUTPUT);

    // bitbang SPI
    pinMode(gcPin_DigiPotSerialData, OUTPUT);
    pinMode(gcPin_DigiPotClock, OUTPUT);
    pinMode(gcPin_DigiPotCS, OUTPUT);

    // interrupt    
    pinMode(gcPin_CANInterruptIn, OUTPUT);
}
