#ifndef COM_I2C_H
#define COM_I2C_H

/** @file com_i2c.h
 *  @brief Function prototypes for the I2C communications subsystem.
 */

#include "types.h"

/**********************/
/**** Preprocessor ****/
/**********************/

/**********************/
/** Global Variables **/
/**********************/

extern T_I2C_ADDRESS gc_I2C_ADDRESS_IMU;

/**********************/
/*** Init Functions ***/
/**********************/

/** \brief Initializes the I2C subsystem. Called from Setup function.
 */
extern void InitializeI2C(void);

/**********************/
/** Logging Functions */
/**********************/


#endif /* COM_I2C_H */