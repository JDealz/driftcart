#ifndef COM_SPI_H
#define COM_SPI_H

/** @file com_spi.h
 *  @brief Function prototypes for the SPI communications subsystem.
 */

#include "types.h"

/**********************/
/**** Preprocessor ****/
/**********************/

/**********************/
/** Global Variables **/
/**********************/

/**********************/
/*** Init Functions ***/
/**********************/

/** \brief Initializes the SPI subsystem using the SPI peripheral. Called from Setup function.
 */
extern void InitializeSPI(void);

/**********************/
/** Logging Functions */
/**********************/


#endif /* COM_SPI_H */