/** @file outputs.cpp
 *  @brief Function definitions for the controller outputs.
 */
#include "outputs.h"
#include "pins.h"

#ifndef TEST_SUITE
#include <Arduino.h>
#else
#include "test/arduinosimulator.h"
#endif
//#include "koolplot.h"

/**********************/
/** Global Variables **/
/**********************/

T_ANALOG_RAWOUT gvCmdThrottlePWM = 0;


T_PWM_DUTY_CYCLE gcPWMLowerBound = 0;

T_PWM_DUTY_CYCLE gcPWMUpperBound = 100;


void InitializeOutputs(void)
{

}

//#ifndef TESTING use this expresssion
//plotdata extern ymotorPWM;

/**
 * Input to this fuction is expected to be a safe value.
 *  
 * \param commandedPWMValue The PWM value to write to the motor.
 */
void writeMotorThrottleDAC(T_ANALOG_RAWOUT commandedPWMValue)
{
    analogWrite(gcPin_DACThrottleOut, (int)commandedPWMValue);
    //#ifndef TESTING use this expresssion
//    ymotorPWM<<commandedPWMValue;
    gvCmdThrottlePWM= (int)commandedPWMValue;
}