/** @file datalogger.cpp
 *  @brief Function definitons for the datalogger.
 *
 *  @note Here we have access to everything. With great power comes great responsibility.
 */
#include <SPI.h>
#include <SD.h>
#include "datalogger.h"

#include "inputs.h"
#include "scaling.h"
#include "outputs.h"
#include "types.h"
#include "batterymonitor.h"
#include "motorcontrol.h"
#include "currentmonitor.h"
#include "fusemonitor.h"
#include "potentiometer.h"
#include "brakingsystem.h"

bool gvSDCardInitalized = false;
T_XBEE_OPERATING_MODE gcXbeeOperatingMode = T_XBEE_OPERATING_MODE::XBEE_OPERATING_MODE_TRANSPARENT;
T_DATA_FORMAT gcDataFormat = T_DATA_FORMAT::DATA_FORMAT_JSON;

String BuildDataString(T_DATA_FORMAT dataFormat)
{
  String dataString = "";

  if (dataFormat == T_DATA_FORMAT::DATA_FORMAT_JSON)
  {
    //dataString+= String("Start of new log session");
    //dataString+= millis();
    dataString+= "{"; //open JSON packet

    dataString += buildJSONDataElement("timestamp", String(millis()), T_JSON_DATA_TYPE::JSON_NUMBER, false); // put a timestamp on the data

    /* Raw Data Inputs */
    dataString += buildJSONDataElement("gvThrottleRaw", String(gvThrottleRaw), T_JSON_DATA_TYPE::JSON_NUMBER, false);
    dataString += buildJSONDataElement("gvHallCurrentRaw", String(gvHallCurrentRaw), T_JSON_DATA_TYPE::JSON_NUMBER, false);

    dataString += buildJSONDataElement("gvArbitratedCurrentReading", String(gvArbitratedCurrentReading), T_JSON_DATA_TYPE::JSON_NUMBER, false);
    dataString += buildJSONDataElement("gvBatteryVoltage", String(gvBatteryVoltage), T_JSON_DATA_TYPE::JSON_NUMBER, false);
    dataString += buildJSONDataElement("gvLowBatteryAlarmActive", gvLowBatteryAlarmActive ? "true" : "false", T_JSON_DATA_TYPE::JSON_BOOL, false);
    dataString += buildJSONDataElement("gvCriticalBatteryAlarmActive", gvCriticalBatteryAlarmActive ? "true" : "false", T_JSON_DATA_TYPE::JSON_BOOL, false);
    dataString += buildJSONDataElement("gvFuseVoltageDrop", String(gvFuseVoltageDrop), T_JSON_DATA_TYPE::JSON_BOOL, false);
    dataString += buildJSONDataElement("gvBrakeApplied", String(gvBrakeApplied), T_JSON_DATA_TYPE::JSON_BOOL, false);
    
    /* Motor Control Stuff */
    dataString += buildJSONDataElement("gvPropulsionEnabled", gvPropulsionEnabled ? "true" : "false", T_JSON_DATA_TYPE::JSON_BOOL, false);
    dataString += buildJSONDataElement("gvThrottleInput", String(gvThrottleInput), T_JSON_DATA_TYPE::JSON_NUMBER, false);
    dataString += buildJSONDataElement("gvThrottleOutputCommanded", String(gvThrottleOutputCommanded), T_JSON_DATA_TYPE::JSON_NUMBER, false);

    /* Raw Data Outputs, depending on the motor control type */
    if (gcMotorControlType == T_MotorControlType::POTENTIOMETER_CONTROL)
    {
      dataString += buildJSONDataElement("gvCmdThrottlePWM", String(gvCmdThrottlePWM), T_JSON_DATA_TYPE::JSON_NUMBER, true);
    }
    else if (gcMotorControlType == T_MotorControlType::DIRECT_VOLTAGE_CONTROL)
    {
      dataString += buildJSONDataElement("gvPotentiometerValue", String(gvPotentiometerValue), T_JSON_DATA_TYPE::JSON_NUMBER, true);
    }  


  //T_MotorControlDisableReason gvMotorControlDisableReason = T_MotorControlDisableReason::NO_REASON;

    dataString += "}"; //close JSON packet

    return dataString;

  }
  else if (dataFormat == T_DATA_FORMAT::DATA_FORMAT_CSV)
  {
    dataString += String(millis()) + ','; // put a timestamp on the data

    /* Raw Data Inputs */
    dataString += String(gvThrottleRaw) + ',';
    dataString += String(gvHallCurrentRaw) + ',';

    dataString += String(gvArbitratedCurrentReading) + ',';
    dataString += String(gvBatteryVoltage) + ',';
    dataString += String(gvLowBatteryAlarmActive) + ',';
    dataString += String(gvCriticalBatteryAlarmActive) + ',';
    dataString += String(gvFuseVoltageDrop) + ',';
    dataString += String(gvBrakeApplied) + ',';

    dataString += String(gvPropulsionEnabled) + ','; //unsure about bool in CSV
    dataString += String(gvThrottleInput) + ',';
    dataString += String(gvThrottleOutputCommanded) + ',';

    
    /* Raw Data Outputs, depending on the motor control type */
    if (gcMotorControlType == T_MotorControlType::POTENTIOMETER_CONTROL)
    {
      dataString += String(gvCmdThrottlePWM);
    }
    else if (gcMotorControlType == T_MotorControlType::DIRECT_VOLTAGE_CONTROL)
    {
      dataString += String(gvPotentiometerValue);
    }  
  }
}

void LogData(bool usbserial, bool xbee, bool sdcard)
{
  /* We may want to generate two data strings at some point. Maybe log in one and use the other for 
  * telemetry, etc.
  */
  String dataString = BuildDataString(gcDataFormat);

  if(usbserial == true){
    Serial.println(dataString);

    Serial.flush();

  }

  if(xbee == true){
    if (gcXbeeOperatingMode == T_XBEE_OPERATING_MODE::XBEE_OPERATING_MODE_TRANSPARENT)
    {
      // If the XBee is in transparent mode, it acts like a simple serial port. Just send the data.
      Serial1.println(dataString);
      Serial1.flush();
    }
    else if (gcXbeeOperatingMode == T_XBEE_OPERATING_MODE::XBEE_OPERATING_MODE_API)
    {
      // If the XBee is in API mode, there is more to do, but also more features.

    }
  }

  if(sdcard == true){
    if(gvSDCardInitalized == false){
      Serial.println("Attempting to log to SD card, but SD card is not initalized");
    }

    // open the file. note that only one file can be open at a time,
    // so you have to close this one before opening another.
    File dataFile = SD.open("datalog.txt", FILE_WRITE);

    // if the file is available, write to it:
    if (dataFile) {
      dataFile.println(dataString);
      dataFile.close();
      // print to the serial port too:
    }
    // if the file isn't open, pop up an error:
    else {
      Serial.println("error opening datalog.txt");
    }
  }
}

void InitializeDataLogger(void)
{
  InitializeSDCard();
  InitializeXbee();
}

void InitializeXbee(void)
{
  Serial1.begin(115200);
}


void InitializeSDCard(void)
{
//make sure this is set properly for the board you are using
    int sdChipSelect=3;


      // Open serial communications and wait for port to open:
  Serial.begin(9600);
  while (!Serial) {
    ; // wait for serial port to connect. Needed for native USB port only
  }


  Serial.print("Initializing SD card...");

  // see if the card is present and can be initialized:
  if (!SD.begin(sdChipSelect)) {
    Serial.println("Card failed, or not present");
    // don't do anything more:
    return;
  }
  Serial.println("card initialized.");

    File dataFile = SD.open("datalog.txt", FILE_WRITE);

    String dataString = "";

    dataString+= String("Start of new log session");
    dataString+= millis(); // put RTC data here.

  // if the file is available, write to it:
  if (dataFile) {
    dataFile.println(dataString);
    dataFile.close();
    // print to the serial port too:
    Serial.println(dataString);
    Serial.flush();
  }

  gvSDCardInitalized = true;

}

String buildJSONDataElement(String varName, String varValue, T_JSON_DATA_TYPE dataType, bool terminalElement)
{  
    String dataString = "";

    dataString+="\"";
    dataString+=varName;
    dataString+="\": ";
    if(dataType == T_JSON_DATA_TYPE::JSON_STRING) // must surround the string with quotes in JSON
    {
      dataString+="\"";
    }
    dataString+= varValue;
    if(dataType == T_JSON_DATA_TYPE::JSON_STRING) // must surround the string with quotes in JSON
    {
      dataString+="\"";
    }
    if(terminalElement == false)
    {
      dataString+=",";
    }
      

    return dataString;
}