/** @file com_bitbang_spi.cpp
 *  @brief Function definitons for the bit banged SPI communications subsystem. 
 * 
 * This module enables SPI communications to occur without using the built-in SPI peripheral. 
 * This has the advantage of configurability and the ability to use any pin, at the expense of speed.
 */
#include "com_bitbang_spi.h"
#include "types.h"
#include "pins.h"
#include "Arduino.h"

/**********************/
/** Global Variables **/
/**********************/

/**********************/
/*** Init Functions ***/
/**********************/

/**********************/
/** Global Functions */
/**********************/

void InitializeBitBangSPI()
{
    digitalWrite(gcPin_DigiPotClock, LOW);
}

void SetChipSelectActive()
{
    digitalWrite(gcPin_DigiPotCS, LOW);
}

void SetChipSelectInactive()
{
    digitalWrite(gcPin_DigiPotCS, HIGH);
}

void bitbangSPIByte(byte b)
{
    // note: make sure clock is correct before sending. May need to force it to a known state?
    shiftOut(gcPin_DigiPotSerialData, gcPin_DigiPotClock, MSBFIRST, b); // verify that LSBFIRST is correct
}
