/** @file motorcontrol.cpp
 *  @brief Definition of Motor Control functions
 * 
 * This file contains the definitions of the motor control algorithm
 * functions for the motor controller
 */

#include "motorcontrol.h"
#include "batterymonitor.h"
#include "brakingsystem.h"
#include "scaling.h"
#include "types.h"

bool gvPropulsionEnabled =  false;
T_MotorControlDisableReason gvMotorControlDisableReason = T_MotorControlDisableReason::NO_REASON;
T_PCT gvThrottleInput = 0.0;

T_MotorType gcMotorType = T_MotorType::BRUSHED_MOTOR;
T_MotorControlType gcMotorControlType = T_MotorControlType::POTENTIOMETER_CONTROL;
bool gcEnableBatteryProtection = false;


void InitializeMotorControl(void)
{
    //attachInterrupt(WHEELPULSE_PIN,countwheelpulse,RISING);
    //
}

void DetermineDriveConditionsMet(void)
{
    if(gcEnableBatteryProtection && getCriticalBatteryAlarmActive())
    {
        gvPropulsionEnabled = false;
        gvMotorControlDisableReason = T_MotorControlDisableReason::LOW_BATTERY_VOLTAGE;
    }
    else // This will always be the default case. Ensure all other cases are covered
    {
        gvPropulsionEnabled = true;
        gvMotorControlDisableReason = T_MotorControlDisableReason::NO_REASON;
    }
}

void ManageMotorSpeed()
{
    
    if (gvPropulsionEnabled){
        if (getBrakeApplied()){
            gvThrottleInput = 0;
            setMotorThrottle(gvThrottleInput,gcMotorType, gcMotorControlType);
        }
        else {
            gvThrottleInput = getThrottle();
            setMotorThrottle(gvThrottleInput, gcMotorType, gcMotorControlType);            
        }

    }

    else{
        // do nothing
    }

}