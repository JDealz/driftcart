#ifndef PINS_H
#define PINS_H

/** @file pins.h
 *  @brief Function prototypes for the controller pins.
 *
 */

#include "types.h"

/**********************/
/**** Preprocessor ****/
/**********************/


/**********************/
/** Global Variables **/
/**********************/

/*
 * Analog Output (one available on Arduino M0 Pro)
 */

/**
 * \brief Output pin used by DAC throttle.
 */
extern T_PIN gcPin_DACThrottleOut;

/*
 * Analog Inputs
 */

/**
 * \brief Input pin used by the hall effect throttle.
 */
extern T_PIN gcPin_HallEffectThrottleIn;

/**
 * \brief Input pin used by the hall effect current sensor.
 */
extern T_PIN gcPin_CurrentSensorHallEffectIn;

/**
 * \brief Input pin used by the current shunt
 */
extern T_PIN gcPin_CurrentShuntAmpedIn;

/**
 * \brief Input pin used by the fuse voltage drop monitor.
 */
extern T_PIN gcPin_FuseVDropAmpedIn;

/**
 * \brief Input pin used by the battery voltage sensor.
 */
extern T_PIN gcPin_TractiveBatteryVoltageSensorIn;

/*
 * Digital Inputs
 */

/**
 * \brief Input pin used by the reverse switch
 */
extern T_PIN gcPin_ReverseSwitchIn;

/**
 * \brief Input pin used by the brake switch
 */
extern T_PIN gcPin_BrakeSwitchIn;

/**
 * \brief Input pin used by Encoder - A
 */
extern T_PIN gcPin_WheelSpeedEncoderAIn;

/**
 * \brief Input pin used by Encoder - B
 */
extern T_PIN gcPin_WheelSpeedEncoderBIn;

/*
 * Fake SPI for Digipot
 */ 

/**
 * \brief Pin used by the Digital Pot for Serial Data
 */
extern T_PIN gcPin_DigiPotSerialData;

/**
 * \brief Pin used by the Digital Pot for Serial Data Clock
 */
extern T_PIN gcPin_DigiPotClock;


/**
 * \brief Output Pin used by PWM Motor Control 5V output
 */
extern T_PIN gcPin_PWMMotorControl5VBoostedOut;

/**
 * \brief Output Pin used by PWM Motor Control Arbitrary voltage output
 */
extern T_PIN gcPin_PWMMotorControlArbOut;

/**
 * \brief Output Pin used by SD Card Chip Select
 */
extern T_PIN gcPin_SDCardChipSelectOut;

/**
 * \brief Output Pin used by CAN tranciever Chip Select
 */
extern T_PIN gcPin_CANChipSelectOut;

/**
 * \brief Output Pin used by Digital Potentiometer Chip Select
 */
extern T_PIN gcPin_DigiPotCS; // digi pot CS?


/**
 * \brief Input Pin used by CAN tranciever interrupt pin
 */
extern T_PIN gcPin_CANInterruptIn;

/**
 * \brief Pin used for UART RxD
 */
extern T_PIN gcPin_RxD;

/**
 * \brief Pin used for UART TxD
 */
extern T_PIN gcPin_TxD;


/**********************/
/*** Init Functions ***/
/**********************/

/**
 * \brief Initialize Pins. Called from Setup
 */
extern void InitializePins(void);

/**
 * \brief Initialize the input pins as inputs.
 */
extern void InitalizeInputPins(void);

/**
 * \brief Initialize the output pins as outputs.
 */
extern void InitalizeOutputPins(void);


/**********************/
/*** Read Functions ***/
/**********************/


#endif /* PINS_H */