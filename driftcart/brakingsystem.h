#ifndef BRAKINGSYSTEM_H
#define BRAKINGSYSTEM_H

/** @file currentmonitor.h
 *  @brief Declaration of Current Monitor functions.
 * 
 * This file contains the declaration of functions used by the current monitor algorithms.
 * 
 * This module is used to collect data about the amount of current being drawn from the batteries,
 * which will directly affect fuse breakdown.
 */

#include "types.h"

/**********************/
/**** Preprocessor ****/
/**********************/

/**********************/
/** Global Variables **/
/**********************/

extern bool gvBrakeApplied;

/**********************/
/**** Calibrations ****/
/**********************/

/**********************/
/*** Init Functions ***/
/**********************/

extern void InitializeBrakingSystem(void);

/**********************/
/** Global Functions **/
/**********************/

extern void MonitorBrakingStatus(void);

extern bool getBrakeApplied(void);


#endif /* CURRENTMONITOR_H */