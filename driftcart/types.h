#ifndef TYPES_H
#define TYPES_H

/** @file types.h
 *  @brief Datatypes for the project.
 */

/**********************/
/**** Preprocessor ****/
/**********************/

/**********************/
/****** Typedefs ******/
/**********************/

/** A simple way to refer to a pin */
typedef int T_PIN;

/** Raw value (counts) from an Analog to Digital Converter (ADC) */
typedef int T_ANALOG_RAWIN;

/** Raw value (duty cycle) sent to a Digital to Analog Converter (DAC) or a PWM output (analogWrite). */
typedef int T_ANALOG_RAWOUT;

/** Percentage */
typedef int T_PCT;

/** Revolutions per minute */
typedef int T_RPM;

/** Amps */
typedef int T_AMP;

/** Volts */
typedef float T_VOLT;

/** ADC Counts */
typedef int T_ADC_COUNTS;

/** DAC Counts */
typedef int T_DAC_COUNTS;

/** Gain */
typedef double T_GAIN;

/** Ohms */
typedef double T_OHMS;

/** Ratio */
typedef double T_RATIO;

/** PWM Duty Cycle */
typedef int T_PWM_DUTY_CYCLE;

/** SPI Device Address */
typedef int T_I2C_ADDRESS;

typedef enum {
  DIRECT_VOLTAGE_CONTROL = 1,
  MOSFET_CONTROL = 2,
  POTENTIOMETER_CONTROL = 3,
  PWM_CONTROL   = 4
} T_MotorControlType;

typedef enum {
    BRUSHED_MOTOR = 1,
    SENSORLESS_BRUSHLESS_MOTOR = 2,
    SENSORED_BRUSHLESS_MOTOR = 3
} T_MotorType;

typedef enum{
    NO_REASON = 0,
    LOW_BATTERY_VOLTAGE = 1
} T_MotorControlDisableReason;

typedef enum{
    JSON_STRING = 1,
    JSON_NUMBER = 2,
    JSON_BOOL = 3
} T_JSON_DATA_TYPE;

typedef enum{
    DATA_FORMAT_JSON = 1,
    DATA_FORMAT_CSV = 2
} T_DATA_FORMAT;

typedef enum{
    XBEE_OPERATING_MODE_TRANSPARENT = 1,
    XBEE_OPERATING_MODE_API = 2
} T_XBEE_OPERATING_MODE;

#endif /* TYPES_H */