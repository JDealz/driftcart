/** @file scaling.cpp
 *  @brief Function defintions for the initial scaling.
 */

#include "scaling.h"
#include <cmath>
#include <cstdlib>
#include "outputs.h"
#include "inputs.h"
#include "potentiometer.h"

#ifndef TEST_SUITE
#include <Arduino.h>
#else
#include "test/arduinosimulator.h"
#endif

//#ifndef TESTING use this expresssion
//#include "koolplot.h"
//plotdata extern ythrottlePCT;

int extern wheelcounts;
int extern oldLoopTime;
int extern currentLoopTime;


T_PCT gvThrottleOutputCommanded = 0;

T_AMP gvCurrentHall=0;

/* cals */

T_VOLT gcVoltRef = 3.3;

T_GAIN gcMainBatteryVoltageGain = 18.13;

T_ADC_COUNTS gcMinADCValue = 0;

T_ADC_COUNTS gcMaxADCValue12Bit = 4095;

T_ADC_COUNTS gcMaxADCValue10Bit = 1023;

T_ADC_COUNTS gcMaxADCValue8Bit = 255;

//TODO: Look at the following cals to determine if they are in the right format. There is some goofy math with current hall effect sensor.
T_GAIN gcShuntCurrentAmpGain = 35;

T_OHMS gcShuntCurrentResistance = 0.00075;

T_ADC_COUNTS gcShuntCurrentCountsAtZeroCurrent = 500;

T_RATIO gcHallCurrentAmpsPerVolt = 10;

T_ADC_COUNTS gcHallCurrentCountsAtZeroCurrent = 500;

T_RPM gcMotorSpeedLimit_Min = 0;

T_RPM gcMotorSpeedLimit_Max = 3000;

T_PCT gcThrottleInputLimit_Min = 0;

T_PCT gcThrottleInputLimit_Max = 100;

T_RATIO gcThrottleOutputGain = 0.5;

T_PCT gcThrottleOutputLimit_Min = 0;

T_PCT gcThrottleOutputLimit_Max = 100;

T_ADC_COUNTS gcRawThrottleInputLimit_Min = 280;

T_ADC_COUNTS gcRawThrottleInputLimit_Max = 800;

T_DAC_COUNTS gcDACValueMin = 0;

T_DAC_COUNTS gcDACValueMax = 255;

T_PWM_DUTY_CYCLE gcPWMThrottleOutputLimit_Min = 0;

T_PWM_DUTY_CYCLE gcPWMThrottleOutputLimit_Max = 1024;

T_ADC_COUNTS gcHallCurrentInputLimit_Min = 10;

T_ADC_COUNTS gcHallCurrentInputLimit_Max = 1024;

int gcDrivenWheelSpeedDivisions = 3;

unsigned int gcPotentiometerMinValueRaw = 0;

//Check potentiometer part number
//MCP4131-103 EP = 7 BIT (128 MAX)
//MCP4131-not 103EP is 8 BIT (256 MAX)
unsigned int gcPotentiometerMaxValueRaw = 128;

/**********************/
/*** Init Functions ***/
/**********************/

void InitializeScaling(void)
{
    // do any init work here
}



/**********************/
/**** getCurrenthall Functions ***/
/**********************/


/** 
 * \returns The throttle percentage. This value should be safe to use.
 */
T_PCT getThrottle()
{
    T_ANALOG_RAWIN lThrottleRaw = readThrottle();

    int lThrottleClipped = clip(lThrottleRaw, gcRawThrottleInputLimit_Min, gcRawThrottleInputLimit_Max);

    T_PCT lThrottleScaled = scaleToRange(lThrottleClipped,
                                       gcRawThrottleInputLimit_Min,
                                       gcRawThrottleInputLimit_Max,
                                       gcThrottleInputLimit_Min,
                                       gcThrottleInputLimit_Max);

    //#ifndef TESTING use this expresssion
    //ythrottlePCT<<lThrottleScaled;                                   

    return lThrottleScaled;
}



/** 
 * \returns instantaneous current consumption in amps from Hall effect sensor. This value should be safe to use.
 */
T_AMP getCurrentHall()
{
    //raw counts of current read
    T_ANALOG_RAWIN lCurrentHallRaw = readCurrentHall();

    //clip the counts to reasonable value (maybe should be clipping the amps conversion?)
    int lCurrentHallClipped = clip(lCurrentHallRaw, gcHallCurrentInputLimit_Min, gcHallCurrentInputLimit_Max);

    //maybe need to define a Counts/Volt constant for analog read??
    T_AMP lCurrentHallamps = (lCurrentHallClipped - gcHallCurrentCountsAtZeroCurrent)*gcHallCurrentAmpsPerVolt/31;

    gvCurrentHall = lCurrentHallamps;

    return lCurrentHallamps;
}


/** 
 * \returns instantaneous current consumption in amps. This value should be safe to use.
 */
T_AMP getCurrentShunt()
{
    //raw counts of current read
    int lCurrentShuntRaw = readCurrentShunt();

    //clip the counts to reasonable value (maybe should be clipping the amps conversion?)
    int lCurrentShuntClipped = clip(lCurrentShuntRaw, gcHallCurrentInputLimit_Min, gcHallCurrentInputLimit_Max);

    float lCurrentShuntvolts=counts2Volts(lCurrentShuntClipped,gcMaxADCValue10Bit,gcShuntCurrentAmpGain);

    float lCurrentShuntamps=lCurrentShuntvolts/gcShuntCurrentResistance;

    return lCurrentShuntamps;
}

T_VOLT getBatteryVoltage()
{
    T_ANALOG_RAWIN lBatteryVoltageRaw = readBatteryVoltage();

    int lBatteryVoltageClipped = clip(lBatteryVoltageRaw, gcMinADCValue, gcMaxADCValue10Bit);

    T_VOLT lBatteryVoltage = counts2Volts(lBatteryVoltageClipped,gcMaxADCValue10Bit,(float)1/gcMainBatteryVoltageGain);

    return lBatteryVoltage;
}

T_VOLT getFuseVoltageDrop()
{
    T_VOLT lFuseVoltageDrop = 0.0;

    T_ANALOG_RAWIN lFuseVoltageDropRaw = readFuseVoltageDrop();
    
    // clip it, scale it.
    lFuseVoltageDrop = lFuseVoltageDropRaw;

    return lFuseVoltageDrop;
}




/** 
 * \returns the Wheelspeed in RPM. This value should be safe to use.
 */

T_RPM getDrivenWheelSpeed()
{
 //wheel counts = pulses from interrupt
 //DRIVEN_WHEELDIVISIONS = # of magnets on wheel 
 int wheelcounts = 0;
 float wheelrotations=wheelcounts/gcDrivenWheelSpeedDivisions;
 int oldLoopTime = 0;
 int oldloopTime = 0;
 #ifndef TEST_SUITE
 int currentLoopTime=millis();
 #else
 int currentLoopTime = 0;
 #endif
 int deltaT= currentLoopTime - oldloopTime;
 T_RPM drivenWheelspeed = wheelrotations/deltaT;
 oldLoopTime=currentLoopTime;
 //do we need to have concern about hitting interrupts while this code is calculating and missing pulses??
 wheelcounts=0;


 return drivenWheelspeed;
}


/**********************/
/**** Set Functions ***/
/**********************/


 /** 
 * Input to this fuction is not expected to be a safe value.
 * 
 * Function should verify commanded value is within hardware capabilities
 * Function should make adjustment based on fuse life
 * Function should lowpass filter the inputs to avoid high rates of change
 *  
 * \param commandedMotorSpeed The speed of the motor (in RPMs).
 */
void setMotorSpeed(T_RPM commandedMotorSpeed, T_MotorType motorType, T_MotorControlType motorControlType)
{

    //adjust motor speed based on fuse life/temperature

    //adjust motor speed command to extend the life of drivetrain and reduce jerk

    //clips motor speed within limits of motor
    int lcmdmotorspeedclipd = clip(commandedMotorSpeed, gcMotorSpeedLimit_Min, gcMotorSpeedLimit_Max);
    
    //scales the motor speed to PWM for output
    int lcmdmotorspeedscaledtopwm = scaleToRange(lcmdmotorspeedclipd,
                                        gcMotorSpeedLimit_Min,
                                        gcMotorSpeedLimit_Max,
                                        gcPWMThrottleOutputLimit_Min,
                                        gcPWMThrottleOutputLimit_Max);
    T_ANALOG_RAWOUT lValue = lcmdmotorspeedscaledtopwm;
    
    if(motorControlType == T_MotorControlType::DIRECT_VOLTAGE_CONTROL){
        
    }
    
}

 /** 
 * Input to this fuction is not expected to be a safe value.
 *  
 * \param commandedMotorThrottle The amount of throttle to apply to the motor(in Percent).
 */
void setMotorThrottle(T_PCT commandedMotorThrottle, T_MotorType motorType, T_MotorControlType motorControlType)
{
    int lcommandedThrottleClippedPCT = clip(commandedMotorThrottle, 0, 100);
    lcommandedThrottleClippedPCT = gcThrottleOutputGain * lcommandedThrottleClippedPCT;
    gvThrottleOutputCommanded = lcommandedThrottleClippedPCT;
    unsigned int lPotValue = 0;
    T_ANALOG_RAWOUT lDACValue = 0;

    switch(motorControlType)
    {
        case T_MotorControlType::POTENTIOMETER_CONTROL:
        lPotValue = scaleToRange(commandedMotorThrottle,
                                        0,
                                        100,
                                        gcPotentiometerMinValueRaw,
                                        gcPotentiometerMaxValueRaw);
        setPotentiometerValue(lPotValue);
        break;

        case T_MotorControlType::DIRECT_VOLTAGE_CONTROL:
        lDACValue = scaleToRange(commandedMotorThrottle,
                                        0,
                                        100,
                                        gcDACValueMin,
                                        gcDACValueMax);
        writeMotorThrottleDAC(lDACValue);
        break;
    }
    
    T_ANALOG_RAWOUT lcmdmotorspeedscaledtorpm = scaleToRange(lcommandedThrottleClippedPCT,
                                        gcThrottleOutputLimit_Min,
                                        gcThrottleOutputLimit_Max,
                                        gcMotorSpeedLimit_Min,
                                        gcMotorSpeedLimit_Max);    
    setMotorSpeed(lcmdmotorspeedscaledtorpm, motorType, motorControlType);
    
}

 
 
 
 
/**********************/
/** Utility Functions */
/**********************/
 
 
 
 /** 
 * Input to this fuction is expected to be a safe value.
 *  
 * \param value The value to scale.
 * \param raw_min The minimum value in the raw signal range.
 * \param raw_max The maximum value in the raw signal range.
 * \param scaled_min The minimum value in the scaled signal range.
 * \param scaled_max The maximum value in the scaled signal range.
 *
 * \returns The scaled value.
 */
int scaleToRange(int value, int raw_min, int raw_max, int scaled_min, int scaled_max)
{
    int raw_range = raw_max-raw_min;
    //cout <<"Rawrange\t"<<raw_range<<endl;
    int scaled_range=scaled_max-scaled_min;
    if(raw_range<=0 || scaled_range<=0)
    {
        //need some sort of error to be thrown, not sure how
    }
    else
    {
        //calculates upper and lower fractions to ensure validity. is this a real test? idk but thought it seemed like a good idea at the time
        //we may want to do something other than cast as int, should we round? floor? ceil?
        double raw_lowfraction=(double)((double)(value-raw_min)/(double)(raw_range));
        double raw_highfraction=(double)((double)(raw_max-value)/(double)(raw_range));
        if(abs(1.0-(raw_lowfraction+raw_highfraction))<.001)
        {
            int scaled_low=(int)(raw_lowfraction*scaled_range+scaled_min);
            int scaled_high=(int)(scaled_max-raw_highfraction*scaled_range);
            value=scaled_low;
        }
        else
        {
        //you done messed up if the two fractions don't = 1
        }

    }

    return value;
}

 /** 
 * Input to this fuction is expected to be a safe value.
 *  
 * \param value The value to scale.
 * \param raw_min The minimum value in the raw signal range.
 * \param raw_max The maximum value in the raw signal range.
 * \param scaled_min The minimum value in the scaled signal range.
 * \param scaled_max The maximum value in the scaled signal range.
 *
 * \returns The scaled value.
 */
double scaleToRange(double value, double raw_min, double raw_max, double scaled_min, double scaled_max)
{
    double raw_range = raw_max-raw_min;
    double scaled_range=scaled_max-scaled_min;

    if(raw_range<=0 || scaled_range<=0)
    {
        //need some sort of error to be thrown, not sure how
    }
    else
    {
        //calculates upper and lower fractions to ensure validity. is this a real test? idk but thought it seemed like a good idea at the time
        double raw_lowfraction=(value-raw_min)/(raw_range);
        double raw_highfraction=(raw_max-value)/(raw_range);
        if(abs(1.0-(raw_lowfraction+raw_highfraction))<.001)
        {
            double scaled_low=raw_lowfraction*scaled_range+scaled_min;
            double scaled_high=scaled_max-raw_highfraction*scaled_range;
            value=scaled_low;
        }
        else
        {
        //you done messed up if the two fractions don't = 1
        }

    }
    return value;
}

/** 
 * \param value The value to operate on.
 * \param lowerBound The lower bound.
 * \param upperBound The upper bound.
 * 
 * \returns The clipped value.
 */
int clip(int value, int lowerBound, int upperBound)
{
    if(value<lowerBound)
        value=lowerBound;
    else if(value>upperBound)
        value=upperBound;
    
    return value;
}

/** 
 * \param value The value to operate on.
 * \param lowerBound The lower bound.
 * \param upperBound The upper bound.
 * 
 * \returns The clipped value.
 */
double clip(double value, double lowerBound, double upperBound)
{
    if(value<lowerBound)
        value=lowerBound;
    else if(value>upperBound)
        value=upperBound;
        
    return value;
}



float counts2Volts(int counts, int channelresolution, float gain)
{
    float tempVolt= (float(counts)/float(channelresolution));
    float volts=  (tempVolt*gcVoltRef)/gain;
    return volts;
}