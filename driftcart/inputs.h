#ifndef INPUTS_H
#define INPUTS_H

/** @file inputs.h
 *  @brief Function prototypes for the controller inputs.
 *
 * @note 
 * All read functions return "unsafe" data. 
 * This means that data returned by these functions must be munged 
 * (clipped, scaled, etc.) before use by an algorithm.
 */



#include "types.h"

/**********************/
/**** Preprocessor ****/
/**********************/


/**********************/
/** Global Variables **/
/**********************/

/**
 * \brief The Raw value of the throttle signal in ADC counts
 */
extern T_ANALOG_RAWIN gvThrottleRaw;

/**
 * \brief The Raw value of the hall effect current sensor signal in ADC counts
 */
extern T_ANALOG_RAWIN gvHallCurrentRaw;

/*
 * \brief The brake applied status
 */
extern bool gvBrakeAppliedRaw;

/**********************/
/*** Init Functions ***/
/**********************/

/** \brief Initialize the inputs module. Called from Setup.
 */
extern void InitializeInputs(void);

/**********************/
/*** Read Functions ***/
/**********************/

/** \brief Gets the raw brake switch value.
 *
 * \return The raw value of the brake switch
 */
extern bool readBrakeSwitch(void);



/** \brief Gets the raw throttle value.
 *
 * \return The raw (unsafe, unscaled) ADC value from the hall effect throttle input.
 */
extern T_ANALOG_RAWIN readThrottle(void);

/** \brief Gets the raw hall effect sensor value.
 *
 * \return The raw (unsafe, unscaled) ADC value from the hall effect current sensor.
 */
extern T_ANALOG_RAWIN readCurrentHall(void);

/** \brief Gets the raw current shunt.
 *
 * \return The raw (unsafe, unscaled) ADC value from the current shunt.
 */
extern T_ANALOG_RAWIN readCurrentShunt(void);

/** \brief Gets the raw battery voltage value.
 *
 * \return The raw (unsafe, unscaled) ADC value from the battery voltage monitor.
 */
extern T_ANALOG_RAWIN readBatteryVoltage(void);

/** \brief Gets the raw fuse voltage drop value.
 *
 * \return The raw (unsafe, unscaled) ADC value from the fuse voltage drop monitor.
 */
extern T_ANALOG_RAWIN readFuseVoltageDrop(void);

/** \brief counts of the wheel speed sensor.
 *
 */
extern void countwheelpulse(void);

#endif /* INPUTS_H */