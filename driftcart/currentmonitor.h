#ifndef CURRENTMONITOR_H
#define CURRENTMONITOR_H

/** @file currentmonitor.h
 *  @brief Declaration of Current Monitor functions.
 * 
 * This file contains the declaration of functions used by the current monitor algorithms.
 * 
 * This module is used to collect data about the amount of current being drawn from the batteries,
 * which will directly affect fuse breakdown.
 */

#include "types.h"

/**********************/
/**** Preprocessor ****/
/**********************/

/**********************/
/** Global Variables **/
/**********************/

/** \brief The reading from the hall effect current sensor.
 */
extern T_AMP gvHallEffectCurrentReading;

/** \brief The reading from the current shunt sensor.
 */
extern T_AMP gvShuntCurrentReading;

/** \brief Arbitrated Current reading, which is filtered based on noise that may be present from one of the sensor sources.
 */
extern T_AMP gvArbitratedCurrentReading;

/**********************/
/**** Calibrations ****/
/**********************/

/**********************/
/*** Init Functions ***/
/**********************/

/** \brief Initialize the current monitoring system. Called from Setup function.
 */
extern void InitializeCurrentMonitorSystem(void);

/**********************/
/** Global Functions **/
/**********************/

/** \brief Monitor battery current. Called from scheduler.
 */
extern void MonitorBatteryCurrent(void);

/** \brief Monitor current using the hall effect sensor. 
 */
extern void MonitorHallEffectCurrent(void);

/** \brief Monitor current using the current shunt sensor. 
 */
extern void MonitorShuntCurrent(void);

/** \brief Arbitrate Current measurement
 */
extern void MonitorTotalCurrent(void);

/** \brief Get the reading from the hall effect sensor
 *  \return Amps.
 */
extern T_AMP getHallEffectCurrentReading(void);

/** \brief Get the reading from the current shunt sensor
 *  \return Amps.
 */
extern T_AMP getShuntCurrentReading(void);

/** \brief Get the arbitrated current value
 *  \return Amps.
 */
extern T_AMP getArbitratedCurrentReading(void);

#endif /* CURRENTMONITOR_H */