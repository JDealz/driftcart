#ifndef FUSEMONITOR_H
#define FUSEMONITOR_H

/** @file fusemonitor.h
 *  @brief Declaration of Fuse monitor functions.
 * 
 * This file contains the declaration of functions used by the fuse monitoring algorithms.
 * 
 * This module is used to collect and analyze data about the state of the fuse. 
 */

#include "types.h"

/**********************/
/**** Preprocessor ****/
/**********************/

/**********************/
/** Global Variables **/
/**********************/

/**
 * \brief The Raw value of the throttle signal in ADC counts
 */
extern T_VOLT gvFuseVoltageDrop;

/**********************/
/**** Calibrations ****/
/**********************/

/**********************/
/*** Init Functions ***/
/**********************/

/** \brief Initialize the fuse monitoring system. Called from Setup function.
 */
extern void InitializeFuseMonitor(void);

/**********************/
/** Global Functions **/
/**********************/

/** \brief Monitor Fuse. Called from the scheduler.
 */
extern void MonitorFuse(void);

#endif /* FUSEMONITOR_H */