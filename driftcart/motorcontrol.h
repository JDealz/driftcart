#ifndef MOTORCONTROL_H
#define MOTORCONTROL_H

/** @file motorcontrol.h
 *  @brief Declaration of Motor Control functions.
 * 
 * This file contains the declaration of functions used by the motor control algorithms.
 */

#include "types.h"

/**********************/
/**** Preprocessor ****/
/**********************/

/**********************/
/** Global Variables **/
/**********************/

/** \brief Indicates if propulsion is enabled
 */
extern bool gvPropulsionEnabled;

/** \brief The reason propulsion is disabled
 */
extern T_MotorControlDisableReason gvMotorControlDisableReason;

/** \brief Throttle input
 */
extern T_PCT gvThrottleInput;

/**********************/
/**   Calibrations   **/
/**********************/

/** \brief Indicates the type of motor being controlled
 */
extern T_MotorType gcMotorType;

/** \brief Indicates the type of motor control being used.
 */
extern T_MotorControlType gcMotorControlType;

/** \brief Set to true to enable battery protection logic
 */
extern bool gcEnableBatteryProtection;

/**********************/
/*** Init Functions ***/
/**********************/

/** \brief Initialize the motor control system. Called from Setup.
 */
extern void InitializeMotorControl(void);

/**********************/
/** Global Functions **/
/**********************/

/** \brief Determines if conditions are met to enable propulsion. Called from scheduler?
 */
extern void DetermineDriveConditionsMet(void);

/** \brief Manages the motor speed. Called from scheduler.
 */
extern void ManageMotorSpeed(void);

#endif /* BRUSHEDMOTORCONTROL_H */