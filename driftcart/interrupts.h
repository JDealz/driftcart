#ifndef INTERRUPTS_H
#define INTERRUPTS_H

/** @file interrupts.h
 *  @brief Function prototypes for the controller interrupts.
 */


#include "types.h"

/**********************/
/**** Preprocessor ****/
/**********************/

/**********************/
/** Global Variables **/
/**********************/

/**********************/
/*** Init Functions ***/
/**********************/

/** \brief Initialize the interrupts. Called from Setup.
 */
extern void InitializeInterrupts(void);

/**********************/
/** Global Functions **/
/**********************/

#endif /* INTERRUPTS_H */