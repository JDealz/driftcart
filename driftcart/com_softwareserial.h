#ifndef COM_SOFWARESERIAL_H
#define COM_SOFWARESERIAL_H

/** @file com_softwareserial.h
 *  @brief Function prototypes for the SoftwareSerial communication subsystem.
 */

#include "types.h"

/**********************/
/**** Preprocessor ****/
/**********************/

/**********************/
/** Global Variables **/
/**********************/

/**********************/
/*** Init Functions ***/
/**********************/

/**********************/
/** Logging Functions */
/**********************/


#endif /* COM_SOFWARESERIAL_H */