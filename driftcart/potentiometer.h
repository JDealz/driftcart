#ifndef POTENTIOMETER_H
#define POTENTIOMETER_H

/** @file potentiometer.h
 *  @brief Function prototypes for the digital potentiometer.
 */

#include "types.h"

/**********************/
/**** Preprocessor ****/
/**********************/

/**********************/
/** Global Variables **/
/**********************/

extern int gvPotentiometerValue;

/**********************/
/*** Init Functions ***/
/**********************/

/**********************/
/** Global Functions */
/**********************/

/**
 * \brief Initialize Digital Potentiometer. Called from Setup
 */
extern void InitializePotentiometer(void);

/** /param value: 0-255: Value of the potentiometer to write
 */
extern void setPotentiometerValue(unsigned int value);


#endif /* POTENTIOMETER_H */