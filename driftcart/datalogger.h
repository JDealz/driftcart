#ifndef DATALOGGER_H
#define DATALOGGER_H

/** @file datalogger.h
 *  @brief Function prototypes for the datalogger.
 */

//NOTE: Here we have access to everything. With great power comes great responsibility.
#include "types.h"
#include <Arduino.h>


/**********************/
/**** Preprocessor ****/
/**********************/


extern T_XBEE_OPERATING_MODE gcXbeeOperatingMode;
extern T_DATA_FORMAT gcDataFormat;

/**********************/
/** Global Variables **/
/**********************/

/** \brief Indicates if the SD card is initalized and ready to be written to. 
 */
extern bool gvSDCardInitalized;

/**********************/
/*** Init Functions ***/
/**********************/

/** \brief Initialize the data logging system. Called from Setup.
 */
extern void InitializeDataLogger(void);

/** \brief Initialize the SD Card.
 */
extern void InitializeSDCard(void);

/** \brief Initalize XBee logging
 */
extern void InitializeXbee(void);


/**********************/
/** Logging Functions */
/**********************/

/** \brief Initialize the data logging system. Called from Setup.
 *  \param Parameter 1 is a boolean indicating if data should be logged to the Serial Port.
 *  \param Parameter 2 is a boolean indicating if data should be logged to the SD Card.
 */
extern void LogData(bool, bool, bool);

/** \brief Builds a JSON data element to be logged.
 *  \param The name of the variable.
 *  \param The value of the variable, represented as a string.
 *  \param The JSON data type (string or number)
 *  \param a boolean indicating if this is the terminal element. If so, a trailing comma will not be added.
 */
extern String buildJSONDataElement(String, String, T_JSON_DATA_TYPE, bool);



#endif /* DATALOGGER_H */