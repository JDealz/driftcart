/** @file inputs.cpp
 *  @brief Function definitons for the controller inputs.
 */

#include "inputs.h"
#include "pins.h"
#ifndef TEST_SUITE
#include <Arduino.h>
#else
#include "test/arduinosimulator.h"
#endif

/**********************/
/** Global Variables **/
/**********************/

T_ANALOG_RAWIN gvThrottleRaw=0;
T_ANALOG_RAWIN gvHallCurrentRaw=-0;
bool gvBrakeAppliedRaw = true;


// "Local" Variables

int wheelcounts=0;
int currentLoopTime=0;
int oldLoopTime=0;

/**********************/
/* Function Definitons */
/**********************/


void InitializeInputs(void)
{

}

bool readBrakeSwitch(void)
{
    int brakeSwitchValue = digitalRead(gcPin_BrakeSwitchIn);
    gvBrakeAppliedRaw = (brakeSwitchValue ? false : true);
    return gvBrakeAppliedRaw;

}

T_ANALOG_RAWIN readThrottle(void)
{
    //read from the pin
    T_ANALOG_RAWIN value = analogRead(gcPin_HallEffectThrottleIn);

    gvThrottleRaw = value;

    return value;
}



T_ANALOG_RAWIN readCurrentHall(void)
{
    T_ANALOG_RAWIN value = analogRead(gcPin_CurrentSensorHallEffectIn);

    gvHallCurrentRaw = value;
    
    return value;
}


T_ANALOG_RAWIN readCurrentShunt(void)
{
    T_ANALOG_RAWIN value = analogRead(gcPin_CurrentShuntAmpedIn);

    return value;    
}


T_ANALOG_RAWIN readBatteryVoltage(void){
    T_ANALOG_RAWIN value = analogRead(gcPin_TractiveBatteryVoltageSensorIn);

    return value;
}

T_ANALOG_RAWIN readFuseVoltageDrop(void)
{
    T_ANALOG_RAWIN value = analogRead(gcPin_FuseVDropAmpedIn);

    return value;

}

void countwheelpulse(void)
{
    wheelcounts++;
}