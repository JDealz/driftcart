#include <Arduino.h>
#include "types.h"
#include "pins.h"
#include "com_can.h"
#include "com_i2c.h"
#include "com_spi.h"
#include "com_bitbang_spi.h"
#include "inputs.h"
#include "outputs.h"
#include "scaling.h"
#include "interrupts.h"
#include "potentiometer.h"
#include "batterymonitor.h"
#include "currentmonitor.h"
#include "fusemonitor.h"
#include "motorcontrol.h"
#include "datalogger.h"
#include "brakingsystem.h"

void setup() {
  InitializePins();
  InitializeBatteryMonitor();
  InitializeCurrentMonitorSystem();
  InitializeFuseMonitor();
  InitializeInputs();
  InitializeOutputs();
  InitializeInterrupts();
  InitializeScaling();

  // Initialize communications subsystems
  InitializeI2C();
  InitializeCAN();
  InitializeSPI();
  InitializeBitBangSPI();

  InitializeMotorControl();
  InitializePotentiometer();
  InitializeDataLogger();
}

void loop() {
/*
  double rawmin=.1;
  double rawmax=4.9;
  double scaledmin=100;
  double scaledmax=1000;
  double test=scaleToRange(incomingByte,rawmin,rawmax,scaledmin,scaledmax);
  */

  ReceiveAndParseCANFrames();

  MonitorBatteryVoltage(); // first determine the battery voltage.

  MonitorBatteryCurrent();
  
  MonitorFuse();

  MonitorBrakingStatus();

  DetermineDriveConditionsMet(); // determine if we have met the conditions to engage drive

  //trying to fix git?
  ManageMotorSpeed(); // Control System for Motor Speeed

  BuildAndSendCANFrames();

  //LogData(serial, sdcard)
  LogData(true, true, false); // Log data from this loop

  delay(50);
  
}
