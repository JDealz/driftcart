import matplotlib as mpl
import numpy as np
import matplotlib.pyplot as plt
import matplotlib.cbook as cbook

data = np.genfromtxt('../throttleResponse.csv', delimiter=',', names=['x', 'y'])

nsamples=[x for x in range(1, data.size+1)]

fig = plt.figure()
fig2 = plt.figure()

ax1 = fig.add_subplot(111)
ax2 = fig2.add_subplot(111)

ax1.set_title("Throttle Analogread Request vs. Throttle PWM Output ")    
ax1.set_xlabel('Samples (N)')
ax1.set_ylabel('')

ax2.set_title("Throttle Analogread Request vs. Throttle PWM Output ")
ax2.set_xlabel('AnalogRead Throttle Req')
ax2.set_ylabel('Throttle PWM Out')

ax1.plot(nsamples, data['x'], color='r', label='Throttle Request Analog Read')
ax1.plot(nsamples, data['y'], color='b', label='Throttle Output PWM')
ax2.plot(data['x'], data['y'], color='b', label='the data')

leg = ax1.legend()

tempx=[]
tempy=[]
tempdiff=[]
for x,y in data.tolist():
    tempx.append(x)
    tempy.append(y)

xslope2=np.diff(tempx,2)
yslope2=np.diff(tempy,2)

inflectionpoints=[]

index=0
for x in yslope2:
    index=index+1
    if abs(x)> 5:
        inflectionpoints.append(index)

ips=[]
for x in inflectionpoints:
    ips.append(tempx[x])
    plt.axvspan(tempx[x],(tempx[x]+1), color='red', alpha=0.5)



plt.show()